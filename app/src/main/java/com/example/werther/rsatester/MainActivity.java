package com.example.werther.rsatester;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import org.apache.commons.codec.binary.ApacheBase64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private static final String M_PUB_KEY =
            "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAITnBZEqWRJfhSwlto3WY16MqRsZvlMgANYjDISFzggrjxGkCcXvW1MICBkNUwbnME4JlBJ8AOL0wP5Le4UlIqcCAwEAAQ==";
    private static final String M_PRI_KEY =
            "MIIBVQIBADANBgkqhkiG9w0BAQEFAASCAT8wggE7AgEAAkEAhOcFkSpZEl" +
            "+FLCW2jdZjXoypGxm+UyAA1iMMhIXOCCuPEaQJxe9bUwgIGQ1TBucwTgmUEnwA4vTA" +
            "/kt7hSUipwIDAQABAkB1NWFdgyGG6KMEKkZzlK2SRy8kbXHC0airMcHqZi8sWBn2Nw" +
            "/NYIng71vDwWBUwK0E9B3NvJxTShKFIhmFdHlxAiEA+Q7GG67UvR05xOkdp1ELr69BcHOsCPtF" +
            "+3yNgVvaKDsCIQCIm2MQPNJaqrtLHpYaSikzZyMMyLF377u266RYygL0hQIhAOMEJW3sUXvD3Plwp5fwVt" +
            "+ftt1igmh5aZPPN0i3ScYrAiB970n/ESMXRKgPU24rZEzhP2Z3hZjdqZtSwZ" +
            "+r3uekhQIhAOblYvvjbAOf3EVEqnmzQjZ23o9Bu9kgMvIzQlC2lvpa";

    public static final String KEY_ALGORITHM = "RSA";
    private static final String CHARSET = "UTF-8";
    private static final int MAX_ENCRYPT_BLOCK = 53;
    private static final int MAX_DECRYPT_BLOCK = 64;
    @Bind(R.id.tv_data)
    TextView tvData;
    @Bind(R.id.tv_encrypt_data)
    TextView tvEncryptData;
    @Bind(R.id.tv_decrypt_data)
    TextView tvDecryptData;
    @Bind(R.id.tv_encode_data)
    TextView tvEncodeData;
    @Bind(R.id.tv_decode_data)
    TextView tvDecodeData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("phone", "15587179788");
            jsonObject.put("name", "罗维特");
            jsonArray.put(jsonObject);
            jsonObject = new JSONObject();
            jsonObject.put("phone", "2312313");
            jsonObject.put("name", "李寻欢");
            jsonArray.put(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String data = jsonArray.toString();

        showData(tvData, "原始数据:", data);

        //         编码
        try {
            data = URLEncoder.encode(data, CHARSET);

            showData(tvEncodeData, CHARSET + "编码后的数据:", data);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // 加密
        String enData = "";
        try {
            enData = encryptByPublicKey(data);
            showData(tvEncryptData, "加密后的数据:", enData);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        // 解密
        if (!TextUtils.isEmpty(enData)) {
            String deData = "";
            try {
                deData = decryptByPrivateKey(enData);
                showData(tvDecryptData, "解密后的数据:", deData);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (InvalidKeySpecException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            // 解码
            try {
                String deCodeData = URLDecoder.decode(deData, CHARSET);
                showData(tvDecodeData, CHARSET + "解码后的数据:", deCodeData);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * 加密数据
     *
     * @param data
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     * @throws IOException
     */
    private String encryptByPublicKey(String data) throws NoSuchAlgorithmException,
            InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException,
            BadPaddingException, IllegalBlockSizeException, IOException {
        byte[] dataBytes = data.getBytes("UTF-8");
        byte[] keyBytes = ApacheBase64.decodeBase64(M_PUB_KEY);//publicKey

        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        Key publicK = keyFactory.generatePublic(x509KeySpec);
        // 对数据加密
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.ENCRYPT_MODE, publicK);
        int inputLen = dataBytes.length;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int offSet = 0;
        byte[] cache;
        int i = 0;
        // 对数据分段加密
        while (inputLen - offSet > 0) {
            if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
                cache = cipher.doFinal(dataBytes, offSet, MAX_ENCRYPT_BLOCK);
            } else {
                cache = cipher.doFinal(dataBytes, offSet, inputLen - offSet);
            }
            out.write(cache, 0, cache.length);
            i++;
            offSet = i * MAX_ENCRYPT_BLOCK;
        }
        byte[] encryptedData = out.toByteArray();
        out.close();

        return ApacheBase64.encodeBase64URLSafeString(encryptedData);
    }

    /**
     * 解密数据
     *
     * @param enData
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     * @throws IOException
     */
    private String decryptByPrivateKey(String enData) throws NoSuchAlgorithmException,
            InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException,
            BadPaddingException, IllegalBlockSizeException, IOException {
        byte[] enDataBytes = ApacheBase64.decodeBase64(enData);
        //        byte[] enDataBytes = enData.getBytes();
        byte[] keyBytes = ApacheBase64.decodeBase64(M_PRI_KEY);//privateKey

        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        Key privateK = keyFactory.generatePrivate(pkcs8KeySpec);
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.DECRYPT_MODE, privateK);
        int inputLen = enDataBytes.length;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int offSet = 0;
        byte[] cache;
        int i = 0;
        // 对数据分段解密
        while (inputLen - offSet > 0) {
            if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
                cache = cipher.doFinal(enDataBytes, offSet, MAX_DECRYPT_BLOCK);
            } else {
                cache = cipher.doFinal(enDataBytes, offSet, inputLen - offSet);
            }
            out.write(cache, 0, cache.length);
            i++;
            offSet = i * MAX_DECRYPT_BLOCK;
        }
        byte[] decryptedData = out.toByteArray();
        out.close();

        return new String(decryptedData);
    }

    private void showData(TextView textView, String label, String data) {
        Log.d("--------RSA--------", label + ":::" + data);
        textView.setText(label + "\n" + data);
    }
}
